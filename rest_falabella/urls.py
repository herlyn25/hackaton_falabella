from django.urls import path, include

from rest_framework.routers import DefaultRouter

from rest_falabella.views import DataFalabellaViewSet

router = DefaultRouter()
router.register(r'falabella', DataFalabellaViewSet)


urlpatterns = [
    path('', include(router.urls)),
]