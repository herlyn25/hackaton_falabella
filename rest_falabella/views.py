from django.shortcuts import render
from rest_framework import viewsets

from rest_falabella.models import DataFalabella
from rest_falabella.serializers import DataFalabellaSerializer

# Create your views here.
class DataFalabellaViewSet(viewsets.ModelViewSet):
    queryset = DataFalabella.objects.all()
    serializer_class = DataFalabellaSerializer
