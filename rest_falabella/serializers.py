from rest_framework import serializers

from rest_falabella.models import DataFalabella

class DataFalabellaSerializer(serializers.ModelSerializer):
    class Meta:
        model = DataFalabella
        fields = '__all__'