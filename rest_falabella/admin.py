from django.contrib import admin

from .models import DataFalabella

@admin.register(DataFalabella)
class DataFalabellaAdmin(admin.ModelAdmin):
    list_display = ['product_id', 'name', 'price', 'delivery_type']
    search_fields = ['product_id', 'name', 'price', 'delivery_type']
