from django.db import models

class DataFalabella(models.Model):
    product_id = models.AutoField(primary_key=True)
    name = models.CharField(max_length=255)
    price = models.CharField(max_length=255)
    delivery_type = models.CharField(max_length=255)
    image_url = models.CharField(max_length=255)

    def __str__(self):
        return self.name
