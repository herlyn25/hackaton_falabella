from django.apps import AppConfig


class RestFalabellaConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'rest_falabella'
